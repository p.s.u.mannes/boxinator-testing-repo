import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// USER PAGES
import { UserMainPageComponent } from './view/pages/user-main-page/user-main-page.component';
import { UserSettingsPageComponent } from './view/pages/user-settings-page/user-settings-page.component';

// GUEST PAGES
import { GuestMainPageComponent } from './view/pages/guest-main-page/guest-main-page.component';

// ADMIN PAGES
import { AdminMainPageComponent } from './view/pages/admin-main-page/admin-main-page.component';
import { AdminModifierPageComponent } from './view/pages/admin-modifier-page/admin-modifier-page.component';

// importing authentication guards
import { AuthGuard } from './utility/app.guard';
import { RoleGuard } from './utility/role-guard.service';

import { LoginPageComponent } from './view/pages/login-page/login-page.component';
import { PackageStatusPageComponent } from './view/pages/package-status-page/package-status-page.component';

const routes: Routes = [
  // ALL
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full',
  },
  // ALL
  {
    path: 'welcome',
    component: LoginPageComponent,
  },
  // USER
  {
    path: 'usermain',
    component: UserMainPageComponent,
    canActivate: [AuthGuard, RoleGuard],
  },
  // ADMIN
  {
    path: 'adminmain',
    component: AdminMainPageComponent,
    canActivate: [AuthGuard],
  },
  // ADMIN
  {
    path: 'adminmodifier',
    component: AdminModifierPageComponent,
    canActivate: [AuthGuard],
  },
  // GUEST
  {
    path: 'guestmain',
    component: GuestMainPageComponent,
  },
  // USER
  {
    path: 'usersettings',
    component: UserSettingsPageComponent,
    canActivate: [AuthGuard],
  },
  // USER
  {
    path: 'shipmenthistory/:id',
    component: PackageStatusPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
