import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGuestShipmentFormComponent } from './add-guest-shipment-form.component';

describe('AddGuestShipmentFormComponent', () => {
  let component: AddGuestShipmentFormComponent;
  let fixture: ComponentFixture<AddGuestShipmentFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddGuestShipmentFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGuestShipmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
