import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { colorValidator } from '../../../validation/color-validator';
import { CountryApiService } from 'src/app/service/country-api/country-api.service';
import { ShipmentApiService } from 'src/app/service/shipment-api/shipment-api.service';
import { Country } from 'src/app/service/country-api/country-interface';
import { HttpErrorResponse } from '@angular/common/http';
import { Shipment } from 'src/app/service/shipment-api/shipment-interface';
import { Router } from '@angular/router';
import { fade } from '../../misc/animations';

@Component({
  selector: 'app-add-guest-shipment-form',
  templateUrl: './add-guest-shipment-form.component.html',
  styleUrls: ['./add-guest-shipment-form.component.css'],
  animations: [fade],
})
export class AddGuestShipmentFormComponent implements OnInit {
  //  Receiver name
  receiverNameValue: string = '';

  //  Weight option (kg)
  weightValue: string = '';

  //  Box colour (colour picker)
  boxColourValue: string = '';

  //  Destination Country
  destinationCountryValue: string = '';

  countryObject: Country | undefined;

  // countryList: string[] = [];
  // countryMap: Map<string, number> = new Map();
  countries: Country[] = [];

  // Map the weight to price
  priceMap: Map<string, number> = new Map([
    ['1', 5],
    ['2', 15],
    ['5', 25],
    ['8', 50],
  ]);

  // Communicating clearly to guest user that
  // their information will be stored and they need to agree
  agreeFlag: boolean = false;
  setAgreeFlag() {
    this.agreeFlag = !this.agreeFlag;
    console.log(this.agreeFlag);
  }

  priceValue: string = '';

  emailValue: string = '';

  constructor(
    private fb: FormBuilder,
    private countryService: CountryApiService,
    private shipmentService: ShipmentApiService,
    private router: Router
  ) {}

  public loadCountries(): void {
    this.countryService.getCountries().subscribe(
      (response: Country[]) => {
        this.countries = response;
      },
      (error: HttpErrorResponse) => alert(error.message)
    );
  }

  public createShipment(): void {
    const guestShipment: Shipment = {
      ...this.addGuestShipmentForm.value,
      account: { email: this.addGuestShipmentForm.value['email'] },
    };
    this.shipmentService.createShipmentForGuest(guestShipment).subscribe(
      (response: Shipment) => {
        // Close the dialog
        this.resetAndCloseDialog();
        this.router.navigate(['/shipmenthistory/' + response.id]);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  ngOnInit(): void {
    this.loadCountries();
  }

  addGuestShipmentForm = this.fb.group({
    agree_flag: [this.agreeFlag, Validators.required],
    receiver_name: [
      this.receiverNameValue,
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        // Not allowing numbers and symbols in name
        Validators.pattern('[^0-9!@#$%^&*]+'),
      ],
    ],

    weight: [this.weightValue, Validators.required],

    box_colour: [
      this.boxColourValue,
      // Custom validator to check if color
      // is valid hexa, rgb, or rgba
      [Validators.required, colorValidator(), Validators.maxLength(25)],
    ],

    destination_id: [this.destinationCountryValue, [Validators.required]],
    cost: [this.priceValue],
    email: [
      this.emailValue,
      [
        Validators.required,
        // Simple light-weight regex to validate emails
        // -> Checks if email starts with letter or number
        // -> Checks for @ and .
        // -> Checks for string at end
        Validators.pattern('[a-zA-Z0-9]+.*@.*\\.[a-zA-Z]+'),
        // -> Checks for tabs whitespaces
        Validators.pattern('[^\\s]+'),
        // According to spec with clarification from IETF RFC Errata in 2010
        Validators.maxLength(254),
      ],
    ],
    // date: [''],
  });

  get boxColour() {
    return this.addGuestShipmentForm.get('box_colour');
  }

  get receiverName() {
    return this.addGuestShipmentForm.get('receiver_name');
  }

  get destinationCountry() {
    return this.addGuestShipmentForm.get('destination_id');
  }

  get weight() {
    return this.addGuestShipmentForm.get('weight');
  }

  get cost() {
    return this.addGuestShipmentForm.get('cost');
  }

  get email() {
    return this.addGuestShipmentForm.get('email');
  }

  public onChangeColor(color: string): void {
    // example from documentation: form.patchValue({first: 'Nancy'});
    this.addGuestShipmentForm.patchValue({ box_colour: color });
  }

  // Resets the form and closes the modal window
  resetAndCloseDialog() {
    // Reset the form to originally fetched API values
    this.boxColourValue = '';
    this.addGuestShipmentForm.reset();
  }

  onSubmit() {
    // Print the value in console
    console.log(this.addGuestShipmentForm.value);

    // Async submit data to API
    this.createShipment();
  }

  get calcPrice() {
    // get the price using the weight and priceMap
    const cost = this.priceMap.get(this.addGuestShipmentForm.value['weight']);
    // get the modifier by filtering the countries object list
    // and taking the modifier value of the first match
    const modifier = this.countries.filter(
      (country) =>
        country.id == this.addGuestShipmentForm.value['destination_id']
    )[0].modifier;

    if (modifier && cost) {
      // Calculate result
      // Round to two decimal places
      // and convert to string with 2 decimal points
      const result = (Math.round(modifier * cost * 100) / 100).toFixed(2);
      // Patch value in form
      this.addGuestShipmentForm.patchValue({
        cost: result,
      });
      // Return result for html display
      return result;
    } else {
      // return null if modifier and cost do not exist
      return null;
    }
  }
}
