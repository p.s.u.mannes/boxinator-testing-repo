import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminShipmentListComponent } from './admin-shipment-list.component';

describe('AdminShipmentListComponent', () => {
  let component: AdminShipmentListComponent;
  let fixture: ComponentFixture<AdminShipmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminShipmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminShipmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
