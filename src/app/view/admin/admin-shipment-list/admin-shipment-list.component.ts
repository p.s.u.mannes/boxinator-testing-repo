import { Component, OnInit } from '@angular/core';
import { fade } from '../../misc/animations';
import { Shipment } from 'src/app/service/shipment-api/shipment-interface';
import { editableShipment } from 'src/app/service/shipment-api/shipment-interface';
import { FormBuilder, Validators } from '@angular/forms';
import { colorValidator } from 'src/app/validation/color-validator';
import { Country } from 'src/app/service/country-api/country-interface';
import { CountryApiService } from 'src/app/service/country-api/country-api.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ShipmentApiService } from 'src/app/service/shipment-api/shipment-api.service';
import { KeycloakService } from 'keycloak-angular';
import { CustomerApiService } from 'src/app/service/customer-api/customer-api.service';
import { Customer } from 'src/app/service/customer-api/customer-interface';

@Component({
  selector: 'app-admin-shipment-list',
  templateUrl: './admin-shipment-list.component.html',
  styleUrls: ['./admin-shipment-list.component.css'],
  animations: [fade],
})
export class AdminShipmentListComponent implements OnInit {
  // Flag to test whether admin has searched
  hasSearched: boolean = false;

  itemsPerPage: number = 5;
  pageNumber: number = 0;
  triggerUpdateMessage: boolean = false;

  // Id value for updated message
  recentUpdateId: number = 0;

  // DATA
  data: Shipment[] = [];
  tempSource: editableShipment[] = [];
  dataSource: editableShipment[] = [];
  tablePageSource: editableShipment[] = [];

  // Flag if editing
  editing: boolean = false;

  status: string = '';

  // LIST OF ERRORS
  updateShipmentErrors: string[] = [];

  // LIST OF COUNTRIES
  countryList: Country[] = [];

  public getCountries(): void {
    this.countryService.getCountries().subscribe(
      (response: Country[]) => {
        this.countryList = response;
      },
      (error: HttpErrorResponse) => alert(error.message)
    );
  }

  getCountryName(id: number): string {
    return this.countryList.filter((country) => country.id == id)[0]
      .country_name;
  }

  public getShipments(): void {
    this.shipmentService.getShipments().subscribe(
      (response: Shipment[]) => {
        this.data = response;

        // FOR EACH ITEM, ADD editing PROPERTY!
        this.dataSource = this.data.map((obj) => ({
          ...obj,
          isEdit: false,
        }));
        console.log(this.dataSource);
        // This code avoids copying the reference value
        this.tempSource = JSON.parse(JSON.stringify(this.dataSource));

        // Get the table data for a single page
        this.getTablePageData();

        // Ensure table is sorted by date at start
        this.sortByCostDesc();
      },
      (error: HttpErrorResponse) => alert(error.message)
    );
  }

  // public updateShipment(id: number, shipment: editableShipment) {
  //   this.shipmentService.updateShipment(id, shipment);
  // }

  constructor(
    private fb: FormBuilder,
    private countryService: CountryApiService,
    private shipmentService: ShipmentApiService,
    private customerService: CustomerApiService,
    private keycloakService: KeycloakService
  ) {}

  ngOnInit(): void {
    // REGISTER ADMIN IF NECESSARY
    this.ensureAdminHasAdminAccess();

    // Get the list of countries
    this.getCountries();

    // Temporary data for filter functions so that the
    // original state is not lost after the API fetch
    // FOR EACH ITEM, ADD editing PROPERTY!
  }

  public updateToAdmin(admin: Customer): void {
    this.customerService.updateCustomer(admin.id, admin).subscribe(
      (response: Customer) => {
        console.log(response);
      },
      (error: HttpErrorResponse) => alert(error.message)
    );
  }

  public ensureAdminHasAdminAccess(): void {
    const email = this.keycloakService.getUsername();
    this.customerService.getCustomerByEmail(email).subscribe(
      (response: Customer) => {
        if (response.account_type != 'ADMINISTRATOR') {
          const admin: Customer = {
            ...response,
            account_type: 'ADMINISTRATOR',
          };
          this.updateToAdmin(admin);
        }
        // Now fetch the admin shipment data
        this.getShipments();
      },
      (error: HttpErrorResponse) => {
        if (error.status != 404) {
          console.log(error.message);
        } else {
          console.log('User email is not registered.');
        }
      }
    );
  }

  updateShipment(shipment: editableShipment): boolean {
    shipment.isEdit = false;

    this.editShipmentForm.setValue({
      receiver_name: shipment.receiver_name,
      weight: shipment.weight,
      box_colour: shipment.box_colour,
      destination_id: shipment.destination_id,
      cost: shipment.cost,
      issue_date: shipment.issue_date,
      status: shipment.status,
    });

    // initialize error array to empty to overwrite potential
    // previous errors
    this.updateShipmentErrors = [];

    // IF GET ERRORS
    if (this.editShipmentForm.invalid) {
      // Reset the data
      this.cancelUpdate(shipment);

      this.updateShipmentErrors.push(
        `Shipment, #ID = ${shipment.id}, could not be updated due to invalid field entries.`
      );

      // receiver name errors
      if (this.editShipmentForm.getError('required', 'receiverName')) {
        this.updateShipmentErrors.push('- Receiver name is required');
      }
      if (this.editShipmentForm.getError('minlength', 'receiverName')) {
        this.updateShipmentErrors.push('- Receiver name is too short');
      } else if (this.editShipmentForm.getError('maxlength', 'receiverName')) {
        this.updateShipmentErrors.push('- Receiver name is too long');
      }
      if (this.editShipmentForm.getError('pattern', 'receiverName')) {
        this.updateShipmentErrors.push(
          '- Receiver name cannot contain numbers and symbols'
        );
      }

      // weight errors
      if (this.editShipmentForm.getError('required', 'weight')) {
        this.updateShipmentErrors.push('- Weight is required');
      }

      // boxColour errors
      if (this.editShipmentForm.getError('required', 'boxColour')) {
        this.updateShipmentErrors.push('- Box colour is required');
      }
      if (this.editShipmentForm.getError('invalid', 'boxColour')) {
        this.updateShipmentErrors.push('- Box colour is not valid');
      }
      if (this.editShipmentForm.getError('maxrgb', 'boxColour')) {
        this.updateShipmentErrors.push(
          '- Exceeded maximum RGB colour value (255)'
        );
      }
      if (this.editShipmentForm.getError('maxalpha', 'boxColour')) {
        this.updateShipmentErrors.push(
          '- Exceeded maximum alpha colour value (1)'
        );
      }

      // destinationCountry errors
      if (this.editShipmentForm.getError('required', 'destinationCountry')) {
        this.updateShipmentErrors.push('- Destination country is required');
      }

      // cost errors
      if (this.editShipmentForm.getError('required', 'cost')) {
        this.updateShipmentErrors.push('- Cost is required');
      }
      if (this.editShipmentForm.getError('maxlength', 'cost')) {
        this.updateShipmentErrors.push('- Cost exceeds max length');
      }
      if (this.editShipmentForm.getError('pattern', 'cost')) {
        this.updateShipmentErrors.push('- Expected number for cost');
      }

      // issueDate errors
      if (this.editShipmentForm.getError('required', 'issueDate')) {
        this.updateShipmentErrors.push('- Issue date is required');
      }

      // status errors
      if (this.editShipmentForm.getError('required', 'status')) {
        this.updateShipmentErrors.push('- Status is required');
      }

      return false;
    }

    // Making sure dataSource and tempSource contain different reference
    // values
    const updateShipmentObj = JSON.parse(JSON.stringify(shipment));

    // Overwrite the changes made by the user in the html
    // by changing dataSource
    this.dataSource = this.dataSource.map((item) => {
      if (item.id == updateShipmentObj.id) {
        item = updateShipmentObj;
      }
      return item;
    });

    // Assign datasource values to tempsource
    // (the local backup of data)
    this.tempSource = JSON.parse(JSON.stringify(this.dataSource));

    // Create return variable for API without the
    // isEdit property
    const returnShipment: Shipment = {
      account: shipment.account,
      box_colour: shipment.box_colour,
      cost: shipment.cost,
      destination_id: shipment.destination_id,
      id: shipment.id,
      issue_date: shipment.issue_date,
      receiver_name: shipment.receiver_name,
      status: shipment.status,
      status_history: shipment.status_history,
      weight: shipment.weight,
    };

    // UPDATE THE API

    this.shipmentService
      .updateShipment(returnShipment.id, returnShipment)
      .subscribe(
        (response: Shipment) => {
          this.triggerUpdateMessage = !this.triggerUpdateMessage;
          this.recentUpdateId = response.id;
        },
        (error: HttpErrorResponse) => {
          console.log('UPDATE ERROR:');
          console.log(error.message), console.log('VALUE USED TO UPDATE');
          // console.log(returnShipment);
          console.log(JSON.stringify(returnShipment));
        }
      );

    // Finally return true
    return true;
  }

  // Retrieve the table data for a single page of the table
  // using a slice of the total data
  getTablePageData() {
    this.tablePageSource = this.dataSource.slice(
      this.pageNumber * this.itemsPerPage,
      this.pageNumber * this.itemsPerPage + this.itemsPerPage
    );
  }

  // NESTING THE FORM - LOGICALLY BREAKING DOWN FORM ELEMENTS
  // When you add a required field to the form control,
  // its initial status is invalid.
  searchOptionForm = this.fb.group({
    searchOption: 'all',
  });

  editShipmentForm = this.fb.group({
    receiver_name: [
      '',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        // Not allowing numbers and symbols in name
        Validators.pattern('[^0-9!@#$%^&*]+'),
      ],
    ],
    weight: [
      '',
      [
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern('\\d+'),
      ],
    ],
    box_colour: [
      '',
      // Custom validator to check if color
      // is valid hexa, rgb, or rgba
      [Validators.required, colorValidator(), Validators.maxLength(25)],
    ],
    destination_id: ['', [Validators.required]],
    cost: [
      '',
      [
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern('\\d+|(\\d+.\\d)'),
      ],
    ],
    issue_date: ['', Validators.pattern('\\d{4}-\\d{2}-\\d{2}')],
    status: ['', Validators.required],
  });

  lastPage: boolean = false;
  firstPage: boolean = true;

  incPage() {
    const maxPages = this.dataSource.length / this.itemsPerPage - 1;
    if (this.pageNumber < maxPages) {
      this.pageNumber += 1;
      this.firstPage = false;
      this.getTablePageData();
    } else {
      this.lastPage = true;
    }
  }

  decPage() {
    if (this.pageNumber > 0) {
      this.pageNumber -= 1;
      this.lastPage = false;
      this.getTablePageData();
    } else {
      this.firstPage = true;
    }
  }

  postFilterProcessing() {
    this.pageNumber = 0;
    this.firstPage = true;
    this.lastPage = false;
    this.getTablePageData();
    // Set the page to 0 if page number is too high
    // after filtering
    // if (this.pageNumber > this.dataSource.length / this.itemsPerPage - 1) {
    //   this.pageNumber = 0;
    // }
  }

  sortByCostAsc() {
    this.dataSource.sort((a, b) => {
      return a.cost - b.cost;
    });
    this.postFilterProcessing();
  }

  sortByCostDesc() {
    this.dataSource.sort((a, b) => {
      return b.cost - a.cost;
    });
    this.postFilterProcessing();
  }

  sortByDateAsc() {
    // YYYY-MM-DD
    this.dataSource.sort((a, b) => {
      return (
        parseInt(a.issue_date.split('-').join('')) -
        parseInt(b.issue_date.split('-').join(''))
      );
    });
    this.postFilterProcessing();
  }

  sortByDateDesc() {
    this.dataSource.sort((a, b) => {
      return (
        parseInt(b.issue_date.split('-').join('')) -
        parseInt(a.issue_date.split('-').join(''))
      );
    });
    this.postFilterProcessing();
  }

  statusMap: Map<string, number> = new Map([
    ['CREATED', 1],
    ['RECEIVED', 2],
    ['INTRANSIT', 3],
    ['COMPLETED', 4],
  ]);

  sortByStatusAsc() {
    this.dataSource.sort((a, b) => {
      return (
        (this.statusMap.get(a.status) || -1) -
        (this.statusMap.get(b.status) || -1)
      );
    });
    this.postFilterProcessing();
  }

  sortByStatusDesc() {
    this.dataSource.sort((a, b) => {
      return (
        (this.statusMap.get(b.status) || -1) -
        (this.statusMap.get(a.status) || -1)
      );
    });
    this.postFilterProcessing();
  }

  sortByColourAsc() {
    this.dataSource.sort((a, b) => {
      return a.box_colour.localeCompare(b.box_colour);
    });
    this.postFilterProcessing();
  }

  sortByColourDesc() {
    this.dataSource.sort((a, b) => {
      return b.box_colour.localeCompare(a.box_colour);
    });
    this.postFilterProcessing();
  }

  sortByNameAsc() {
    this.dataSource.sort((a, b) => {
      // Forcing a to be a string to avoid exceptions
      return a.receiver_name.localeCompare(b.receiver_name);
    });
    this.postFilterProcessing();
  }

  sortByNameDesc() {
    this.dataSource.sort((a, b) => {
      return b.receiver_name.localeCompare(a.receiver_name);
    });
    this.postFilterProcessing();
  }

  sortByWeightAsc() {
    this.dataSource.sort((a, b) => {
      // Forcing a to be a string to avoid exceptions
      return a.weight - b.weight;
    });
    this.postFilterProcessing();
  }

  sortByWeightDesc() {
    this.dataSource.sort((a, b) => {
      return b.weight - a.weight;
    });
    this.postFilterProcessing();
  }

  //@TODO USE THE COUNTRY NAME AND NOT THE ID
  //OR CONVERT ID TO INT
  sortByDestinationAsc() {
    this.dataSource.sort((a, b) => {
      // Forcing a to be a string to avoid exceptions
      return a.destination_id - b.destination_id;
    });
    this.postFilterProcessing();
  }

  sortByDestinationDesc() {
    this.dataSource.sort((a, b) => {
      return b.destination_id - a.destination_id;
    });
    this.postFilterProcessing();
  }

  // Cancel update action
  cancelUpdate(shipment: editableShipment) {
    // Get the specific 'saved' shipment from tempSource
    const tempShipment = this.tempSource.filter(
      (val) => val.id == shipment.id
    )[0];

    // Making sure dataSource and tempSource contain different reference
    // values
    const resultShipment = JSON.parse(JSON.stringify(tempShipment));
    // Ensuring the shipment cannot be edited and the editor is closed
    resultShipment.isEdit = false;

    // Overwrite the changes made by the user in the html
    // by changing dataSource
    this.dataSource = this.dataSource.map((item) => {
      if (item.id == resultShipment.id) {
        item = resultShipment;
      }
      return item;
    });

    this.getTablePageData();
  }

  filterTable(e: any) {
    // The admin has searched, show errors in future for
    // empty table
    this.hasSearched = true;

    const searchString = e.target.value.toLowerCase();

    // Reset the data
    this.dataSource = JSON.parse(JSON.stringify(this.tempSource));

    // IF FILTER BY ID IS CHECKED
    if (this.searchOptionForm.value['searchOption'].includes('id')) {
      this.dataSource = this.dataSource.filter((shipment) =>
        shipment.id.toString().includes(searchString)
      );
    }

    // IF FILTER BY STATUS IS CHECKED
    if (this.searchOptionForm.value['searchOption'].includes('status')) {
      this.dataSource = this.dataSource.filter((shipment) =>
        shipment.status.toLowerCase().includes(searchString)
      );
    }

    // IF FILTER BY DATE IS CHECKED
    if (this.searchOptionForm.value['searchOption'].includes('date')) {
      this.dataSource = this.dataSource.filter((shipment) =>
        shipment.issue_date.toLowerCase().includes(searchString)
      );
    }

    // IF FILTER BY ALL IS CHECKED
    if (this.searchOptionForm.value['searchOption'].includes('all')) {
      this.dataSource = this.dataSource.filter(
        (shipment) =>
          shipment.id.toString().includes(searchString) ||
          shipment.issue_date.toLowerCase().includes(searchString) ||
          shipment.status.toLowerCase().includes(searchString)
      );
    }

    this.postFilterProcessing();
  }
}
