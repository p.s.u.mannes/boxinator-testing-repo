import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShipmentApiService } from 'src/app/service/shipment-api/shipment-api.service';
import { Shipment } from 'src/app/service/shipment-api/shipment-interface';

@Component({
  selector: 'app-package-status',
  templateUrl: './package-status.component.html',
  styleUrls: ['./package-status.component.css'],
})
export class PackageStatusComponent implements OnInit {
  //the shipment to be rendered in the view
  shipment: Shipment | undefined;

  constructor(
    private shipmentService: ShipmentApiService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const shipmentId = this.route.snapshot.paramMap.get('id');
    if (shipmentId) {
      //get and populate the shipment to be rendered
      this.getShipment(parseInt(shipmentId));
    }
  }

  public getShipment(id: number): void {
    this.shipmentService.getShipmentById(id).subscribe(
      (response: Shipment) => {
        //populate the shipment with the values from the API
        this.shipment = response;
      },
      (error: HttpErrorResponse) => alert(error.message)
    );
  }
}
