import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.css'],
})
export class AdminNavbarComponent implements OnInit {
  constructor(private keycloakService: KeycloakService) {}

  ngOnInit(): void {}

  logout() {
    this.keycloakService.logout(window.location.origin + '/welcome');
  }
}
