import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminModifiersComponent } from './admin-modifiers.component';

describe('AdminModifiersComponent', () => {
  let component: AdminModifiersComponent;
  let fixture: ComponentFixture<AdminModifiersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminModifiersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminModifiersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
