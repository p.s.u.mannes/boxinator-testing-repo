import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

// CREATING A FORM GROUP
// a form group instance tracks
// the form state of a group of form control instances
import { FormBuilder, Validators } from '@angular/forms';
import { CountryApiService } from 'src/app/service/country-api/country-api.service';
import { Country } from 'src/app/service/country-api/country-interface';
import { fade } from '../../misc/animations';
// Country data with modifier values

@Component({
  selector: 'app-admin-modifiers',
  templateUrl: './admin-modifiers.component.html',
  styleUrls: ['./admin-modifiers.component.css'],
  animations: [fade],
})
export class AdminModifiersComponent implements OnInit {
  recentChange: string = '';

  countries: Country[] = [];

  public getCountries(): void {
    this.countryService.getCountries().subscribe(
      (response: Country[]) => {
        this.countries = response;
      },
      (error: HttpErrorResponse) => alert(error.message)
    );
  }

  public updateCountry(id: number, country: Country) {
    this.countryService.updateCountry(id, country).subscribe(
      (response: Country) => {
        // inform of recent change
        this.recentChange =
          'Recent update: ' +
          country.country_name +
          ' cost modifier changed to ' +
          country.modifier;
        setTimeout(() => {
          this.recentChange = '';
        }, 3500);

        // Update the local list of countries
        this.countries.filter((country) => country.id == response.id)[0] =
          response;
      },
      (error: HttpErrorResponse) => alert(error.message)
    );
  }

  constructor(
    private fb: FormBuilder,
    private countryService: CountryApiService
  ) {}

  ngOnInit(): void {
    this.getCountries();
  }

  adminModifierForm = this.fb.group({
    country: ['', [Validators.required]],
    modifier: [
      '',
      [
        Validators.required,
        Validators.maxLength(6),
        // Expected modifiers in the form of float or int
        Validators.pattern('(\\d+.\\d+)|(\\d+)'),
      ],
    ],
    id: '',
  });

  adjustFormValues() {
    this.adminModifierForm.patchValue({
      modifier: this.adminModifierForm.value['country'].modifier,
    });
  }

  get country() {
    return this.adminModifierForm.value['country'];
  }

  get modifier() {
    return this.adminModifierForm.value['modifier'];
  }

  resetForm() {
    // Reset the form to originally fetched API values
    this.adminModifierForm.patchValue({ country: '' });
    this.adminModifierForm.patchValue({ modifier: '' });
    this.adminModifierForm.markAsPristine();
    this.adminModifierForm.markAsUntouched();
  }

  // @TODO NO idea why this is not working,
  // countries not listed in select alphabetically
  get countriesSorted() {
    return this.countries.sort((a, b) => {
      return a.country_name.localeCompare(b.country_name);
    });
  }

  onSubmit() {
    this.recentChange = '';
    // Configure what to send to API
    const id = this.adminModifierForm.value['country'].id;
    const country: Country = {
      id: id,
      country_name: this.adminModifierForm.value['country'].country_name,
      modifier: this.adminModifierForm.value['modifier'],
    };

    // Async update country
    this.updateCountry(id, country);

    // Update local variables
    this.countries.filter((val) => val.id == id)[0].modifier = country.modifier;

    // Reset the form
    this.resetForm();
  }
}
