import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-navbar-package-history',
  templateUrl: './navbar-package-history.component.html',
  styleUrls: ['./navbar-package-history.component.css'],
})
export class NavbarPackageHistoryComponent implements OnInit {
  constructor(private router: Router, private keycloak: KeycloakService) {}

  ngOnInit(): void {}

  flexibleRedirect() {
    if (this.keycloak.isUserInRole('user')) {
      this.router.navigate(['/', 'usermain']);
    } else {
      this.router.navigate(['/guestmain']);
    }
  }
}
