import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarPackageHistoryComponent } from './navbar-package-history.component';

describe('NavbarPackageHistoryComponent', () => {
  let component: NavbarPackageHistoryComponent;
  let fixture: ComponentFixture<NavbarPackageHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarPackageHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarPackageHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
