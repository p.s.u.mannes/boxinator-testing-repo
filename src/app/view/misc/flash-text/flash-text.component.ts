import { Component, Input, OnInit } from '@angular/core';
import { fade } from '../animations';

@Component({
  selector: 'app-flash-text',
  templateUrl: './flash-text.component.html',
  styleUrls: ['./flash-text.component.css'],
  animations: [fade],
})
export class FlashTextComponent implements OnInit {
  @Input() text: string = '';
  // Used to trigger ngOnChanges
  @Input() triggerChange: boolean = false;

  initialized = false;
  show: boolean = false;

  constructor() {}

  ngOnInit(): void {
    this.initialized = true;
  }

  ngOnChanges() {
    // execute only after ngOnInit has been called
    if (this.initialized) {
      // Get the current value of the @Input whenever
      // changes are triggered by @Input triggerChange
      // and then handle the input
      if (this.text && this.text.length > 0) {
        // show the text
        this.show = true;

        // hide the text again
        // after a timeout
        setTimeout(() => {
          this.show = false;
        }, 1500);
      }
    }
  }
}
