import { Component, Input, OnInit } from '@angular/core';
import { shrink } from '../animations';

@Component({
  selector: 'app-clickable-icon',
  templateUrl: './clickable-icon.component.html',
  styleUrls: ['./clickable-icon.component.css'],
  animations: [shrink],
})
export class ClickableIconComponent implements OnInit {
  // define the type of symbol to use
  // e.g. &#9650;
  @Input() icon: string = 'arrow_drop_down_circle';

  initialized = false;
  state = 'unhighlighted';

  constructor() {}

  ngOnInit(): void {
    this.initialized = true;
  }

  triggerAnimation() {
    // Get the current value of the @Input whenever
    // changes are triggered by @Input triggerChange
    // and then handle the input
    if (this.icon && this.icon.length > 0) {
      // shrink the icon on active click
      this.state = 'active';

      // grow the icon back to normal
      // after a timeout
      setTimeout(() => {
        this.state = 'unhighlighted';
      }, 150);
    }
  }
}
