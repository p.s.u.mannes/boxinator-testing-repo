import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { CustomerApiService } from 'src/app/service/customer-api/customer-api.service';

@Component({
  selector: 'app-navbar-settings',
  templateUrl: './navbar-settings.component.html',
  styleUrls: ['./navbar-settings.component.css'],
})
export class NavbarSettingsComponent implements OnInit {
  customerExists: boolean = false;

  constructor(
    private keycloakService: KeycloakService,
    private customerService: CustomerApiService
  ) {}

  ngOnInit(): void {
    // Get the username (configured as email in the keycloak realm) from the authenticated user
    const email = this.keycloakService.getUsername();

    // Load form with values if user is already registered
    this.checkCustomer(email);
  }

  logout() {
    this.keycloakService.logout(window.location.origin + '/welcome');
  }

  checkCustomer(email: string): void {
    this.customerService.getCustomerByEmail(email).subscribe(
      (response: any) => {
        this.customerExists = true;
      },
      (error: any) => {
        this.customerExists = false;
      }
    );
  }
}
