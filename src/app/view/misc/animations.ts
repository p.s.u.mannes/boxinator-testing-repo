import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

/**
 * fade in and fade out with Y-axis offset
 * on enter and leave
 * @TODO apply to modal
 */
export const fade = trigger('fade', [
  transition(':enter', [
    style({ opacity: 0, transform: 'translateY(10px)' }),
    animate('500ms', style({ opacity: 1, transform: 'translateY(0)' })),
  ]),
  transition(':leave', [
    animate('500ms', style({ opacity: 0, transform: 'translateY(10px)' })),
  ]),
]);

/**
 * quickly shrink an item depending on whether
 * item is clicked, hovered over or not in focus
 */
export const shrink = trigger('shrink', [
  state(
    'active',
    style({
      fontSize: '1.4rem',
      padding: '0.3rem',
      opacity: '0.8',
      cursor: 'wait',
    })
  ),
  state('highlighted', style({ fontSize: '1.6rem', cursor: 'pointer' })),
  state(
    'unhighlighted',
    style({
      fontSize: '1.8rem',
      cursor: 'pointer',
    })
  ),

  transition('highlighted => unhighlighted', animate('20ms ease-in')),
  transition('unhighlighted => highlighted', animate('20ms ease-in')),
  transition('active => unhighlighted', animate('100ms ease-in')),
]);
