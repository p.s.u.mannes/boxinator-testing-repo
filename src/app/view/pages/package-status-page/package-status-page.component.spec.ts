import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageStatusPageComponent } from './package-status-page.component';

describe('PackageStatusPageComponent', () => {
  let component: PackageStatusPageComponent;
  let fixture: ComponentFixture<PackageStatusPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackageStatusPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageStatusPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
