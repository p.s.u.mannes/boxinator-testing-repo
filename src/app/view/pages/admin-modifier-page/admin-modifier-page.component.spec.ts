import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminModifierPageComponent } from './admin-modifier-page.component';

describe('AdminModifierPageComponent', () => {
  let component: AdminModifierPageComponent;
  let fixture: ComponentFixture<AdminModifierPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminModifierPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminModifierPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
