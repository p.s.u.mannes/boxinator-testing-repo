import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestMainPageComponent } from './guest-main-page.component';

describe('GuestMainPageComponent', () => {
  let component: GuestMainPageComponent;
  let fixture: ComponentFixture<GuestMainPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuestMainPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
