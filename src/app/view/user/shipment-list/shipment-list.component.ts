import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
// CREATING A FORM GROUP
// a form group instance tracks
// the form state of a group of form control instances
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ShipmentApiService } from 'src/app/service/shipment-api/shipment-api.service';
import { Shipment } from 'src/app/service/shipment-api/shipment-interface';

@Component({
  selector: 'app-shipment-list',
  templateUrl: './shipment-list.component.html',
  styleUrls: ['./shipment-list.component.css'],
})
export class ShipmentListComponent implements OnInit {
  tempSource: Shipment[] = [];
  dataSource: Shipment[] = [];

  // initialize flag whether user clicked search
  // to not show no results error for new users
  hasSearched: boolean = false;

  constructor(
    private fb: FormBuilder,
    private shipmentService: ShipmentApiService,
    private router: Router
  ) {}

  public getShipments(): void {
    this.shipmentService.getShipments().subscribe(
      (response: Shipment[]) => {
        this.dataSource = response;
        this.tempSource = response;
      },
      (error: HttpErrorResponse) => console.log(error.message)
    );
  }

  ngOnInit(): void {
    // FETCH DATA FROM API USING SERVICE ONINIT
    this.getShipments();

    // Ensure table is sorted by date at start
    this.sortByCostDesc();
  }

  // Refetch API data
  refresh(): void {
    // FETCH DATA FROM API USING SERVICE ONINIT
    this.getShipments();

    // Ensure table is sorted by date at start
    this.sortByCostDesc();
  }

  navigateToHistory(id: number): void {
    this.router.navigateByUrl(`/shipmenthistory/${id}`);
  }

  // NESTING THE FORM - LOGICALLY BREAKING DOWN FORM ELEMENTS
  // When you add a required field to the form control,
  // its initial status is invalid.
  searchOptionForm = this.fb.group({
    searchOption: 'all',
  });

  sortByCostAsc() {
    this.dataSource.sort((a, b) => {
      return a.cost - b.cost;
    });
  }

  sortByCostDesc() {
    this.dataSource.sort((a, b) => {
      return b.cost - a.cost;
    });
  }

  sortByColourAsc() {
    this.dataSource.sort((a, b) => {
      return a.box_colour.localeCompare(b.box_colour);
    });
  }

  sortByColourDesc() {
    this.dataSource.sort((a, b) => {
      return b.box_colour.localeCompare(a.box_colour);
    });
  }

  sortByDateAsc() {
    // YYYY-MM-DD
    this.dataSource.sort((a, b) => {
      return (
        parseInt(a.issue_date.split('-').join('')) -
        parseInt(b.issue_date.split('-').join(''))
      );
    });
  }

  sortByDateDesc() {
    this.dataSource.sort((a, b) => {
      return (
        parseInt(b.issue_date.split('-').join('')) -
        parseInt(a.issue_date.split('-').join(''))
      );
    });
  }

  statusMap: Map<string, number> = new Map([
    ['CREATED', 1],
    ['RECEIVED', 2],
    ['INTRANSIT', 3],
    ['COMPLETED', 4],
  ]);

  sortByStatusAsc() {
    this.dataSource.sort((a, b) => {
      return (
        (this.statusMap.get(a.status) || -1) -
        (this.statusMap.get(b.status) || -1)
      );
    });
  }

  sortByStatusDesc() {
    this.dataSource.sort((a, b) => {
      return (
        (this.statusMap.get(b.status) || -1) -
        (this.statusMap.get(a.status) || -1)
      );
    });
  }

  filterTable(e: any) {
    // set has searched flag to true
    // to show html errors if errors exist
    this.hasSearched = true;

    let searchString = e.target.value.toLowerCase();
    this.dataSource = this.tempSource;

    // IF FILTER BY ID IS CHECKED
    if (this.searchOptionForm.value['searchOption'].includes('id')) {
      this.dataSource = this.dataSource.filter((shipment) =>
        shipment.id.toString().includes(searchString)
      );
    }

    // IF FILTER BY STATUS IS CHECKED
    if (this.searchOptionForm.value['searchOption'].includes('status')) {
      this.dataSource = this.dataSource.filter((shipment) =>
        shipment.status.toLowerCase().includes(searchString)
      );
    }

    // IF FILTER BY DATE IS CHECKED
    if (this.searchOptionForm.value['searchOption'].includes('date')) {
      this.dataSource = this.dataSource.filter((shipment) =>
        shipment.issue_date.toLowerCase().includes(searchString)
      );
    }

    // IF FILTER BY ALL IS CHECKED
    if (this.searchOptionForm.value['searchOption'].includes('all')) {
      this.dataSource = this.dataSource.filter(
        (shipment) =>
          shipment.id.toString().includes(searchString) ||
          shipment.issue_date.toLowerCase().includes(searchString) ||
          shipment.status.toLowerCase().includes(searchString)
      );
    }
  }
}
