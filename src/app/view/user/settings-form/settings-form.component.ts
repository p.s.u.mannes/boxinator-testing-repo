import { Component, OnInit } from '@angular/core';
// CREATING A FORM GROUP
// a form group instance tracks
// the form state of a group of form control instances
import { FormBuilder, Validators } from '@angular/forms';

// NO LONGER NEEDED IF FORM BUILDER IS USED:
// import { FormGroup, FormControl } from '@angular/forms';

// Reactive forms include a set of validator functions
// for common use cases. These functions receive a control
// to validate against and return an error object
// or a null value based on the validation check.
import { dobValidator } from '../../../validation/dob-validator';
import { CustomerApiService } from 'src/app/service/customer-api/customer-api.service';
import { Customer } from 'src/app/service/customer-api/customer-interface';
import { HttpErrorResponse } from '@angular/common/http';
import { CountryApiService } from 'src/app/service/country-api/country-api.service';
import { Country } from 'src/app/service/country-api/country-interface';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-settings-form',
  templateUrl: './settings-form.component.html',
  styleUrls: ['./settings-form.component.css'],
})
export class SettingsFormComponent implements OnInit {
  firstNameValue: string = '';
  lastNameValue: string = '';
  emailValue: string = '';
  dobValue: string = '';
  countryValue: string = '';
  zipCodeValue: string = '';
  phoneValue: string = '';
  customerExists: boolean = false;

  countryList: Country[] = [];

  initialValues: any;

  public getCountries(): void {
    this.countryService.getCountries().subscribe(
      (response: Country[]) => {
        this.countryList = response;
      },
      (error: HttpErrorResponse) => console.log(error.message)
    );
  }

  public createCustomer(customer: Customer): void {
    this.customerService.createCustomer(customer).subscribe(
      (response: Customer) => {
        console.log(response);
        this.resetForm();
        this.router.navigate(['/usermain']);
      },
      (error: HttpErrorResponse) => console.log(error.message)
    );
  }

  public updateCustomer(customer: Customer): void {
    this.customerService.updateCustomer(customer.id, customer).subscribe(
      (response: Customer) => {
        console.log(response);
        this.resetForm();
        this.router.navigate(['/usermain']);
      },
      (error: HttpErrorResponse) => alert(error.message)
    );
  }

  public getCustomer(email: string): void {
    this.customerService.getCustomerByEmail(email).subscribe(
      (response: Customer) => {
        this.profileForm.get('id')?.setValue(response.id);
        this.profileForm.get('first_name')?.setValue(response.first_name);
        this.profileForm.get('last_name')?.setValue(response.last_name);
        this.profileForm.get('email')?.setValue(response.email);
        this.profileForm.get('birthday')?.setValue(response.birthday);
        this.profileForm.get('country_id')?.setValue(response.country_id);
        this.profileForm.get('zip_code')?.setValue(response.zip_code);
        this.profileForm.get('phone_number')?.setValue(response.phone_number);

        // Flag used in Submit form to either
        // create or update customer
        this.customerExists = true;
      },
      (error: HttpErrorResponse) => {
        if (error.status != 404) {
          console.log(error.message);
        } else {
          console.log('User email is not registered.');
        }
      }
    );
  }

  // using form builder to improve excess new FormGroup code
  constructor(
    private fb: FormBuilder,
    private customerService: CustomerApiService,
    private countryService: CountryApiService,
    private router: Router,
    private keycloakService: KeycloakService
  ) {}

  ngOnInit(): void {
    this.getCountries();

    // Get the username (configured as email in the keycloak realm) from the authenticated user
    const email = this.keycloakService.getUsername();

    // Load form with values if user is already registered
    this.getCustomer(email);

    // Assign initial form values to API data
    this.profileForm.get('email')?.setValue(email);

    this.initialValues = this.profileForm.value;
  }

  // NESTING THE FORM - LOGICALLY BREAKING DOWN FORM ELEMENTS
  // When you add a required field to the form control,
  // its initial status is invalid.
  profileForm = this.fb.group({
    id: [''],
    first_name: [
      this.firstNameValue,
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        // Not allowing numbers and symbols in name
        Validators.pattern('[^0-9!@#$%^&*]+'),
      ],
    ],
    last_name: [
      this.lastNameValue,
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        // Not allowing numbers and symbols in name
        Validators.pattern('[^0-9!@#$%^&*]+'),
      ],
    ],
    email: [
      this.emailValue,
      [
        Validators.required,
        // Simple light-weight regex to validate emails
        // -> Checks if email starts with letter or number
        // -> Checks for @ and .
        // -> Checks for string at end
        Validators.pattern('[a-zA-Z0-9]+.*@.*\\.[a-zA-Z]+'),
        // -> Checks for tabs whitespaces
        Validators.pattern('[^\\s]+'),
        // According to spec with clarification from IETF RFC Errata in 2010
        Validators.maxLength(254),
      ],
    ],
    birthday: [
      this.dobValue,
      [Validators.required, dobValidator(), Validators.maxLength(10)],
    ],
    country_id: [this.countryValue, [Validators.required]],
    zip_code: [
      this.zipCodeValue,
      [Validators.required, Validators.maxLength(9)],
    ],
    // Max length 15 chars for phone according to ITU-T
    phone_number: [
      this.phoneValue,
      [
        // Require input field
        Validators.required,
        Validators.minLength(6),
        // Protect input field against excessive length
        Validators.maxLength(128),
        /* Check if the user entered numbers...
        However liberally do not enforce phone number
        BECAUSE REASON:
        since the only true validation would be over text
        message confirmation and users may want to e.g. type
        "123 456 7890 until 6pm, then 098 765 4321"
        or not disclose their number at all, in which case
        they can always put in a fake number */
        Validators.pattern('.*[\\d]+.*'),
      ],
    ],
  });

  get firstName() {
    return this.profileForm.get('first_name');
  }

  get lastName() {
    return this.profileForm.get('last_name');
  }

  get email() {
    return this.profileForm.get('email');
  }

  // the parsed value is always formatted yyyy-mm-dd.
  get dob() {
    return this.profileForm.get('birthday');
  }

  get country() {
    return this.profileForm.get('country_id');
  }

  get zipCode() {
    return this.profileForm.get('zip_code');
  }

  get phone() {
    return this.profileForm.get('phone_number');
  }

  resetForm() {
    // Reset the form to originally fetched API values
    this.profileForm.reset(this.initialValues);
  }

  onSubmit() {
    let accountTypeValue = 'REGISTERED_USER';
    if (this.keycloakService.isUserInRole('admin')) {
      accountTypeValue = 'ADMINISTRATOR';
    }
    const customer: Customer = {
      ...this.profileForm.value,
      account_type: accountTypeValue,
    };
    // IF CUSTOMER EXIST ASYNC UPDATE
    if (this.customerExists) {
      this.updateCustomer(customer);
    }
    // ELSE ASYNC CREATE
    else {
      this.createCustomer(customer);
    }
  }
}
