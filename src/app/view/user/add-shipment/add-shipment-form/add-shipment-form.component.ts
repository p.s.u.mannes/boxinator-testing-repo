import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { colorValidator } from '../../../../validation/color-validator';
import { MatDialogRef } from '@angular/material/dialog';
import { AddShipmentModalComponent } from '../add-shipment-modal/add-shipment-modal.component';
import { CountryApiService } from 'src/app/service/country-api/country-api.service';
import { ShipmentApiService } from 'src/app/service/shipment-api/shipment-api.service';
import { Country } from 'src/app/service/country-api/country-interface';
import { HttpErrorResponse } from '@angular/common/http';
import { Shipment } from 'src/app/service/shipment-api/shipment-interface';

@Component({
  selector: 'app-add-shipment-form',
  templateUrl: './add-shipment-form.component.html',
  styleUrls: ['./add-shipment-form.component.css'],
})
export class AddShipmentFormComponent implements OnInit {
  //  Receiver name
  receiverNameValue: string = '';

  //  Weight option (kg)
  weightValue: string = '';

  //  Box colour (colour picker)
  boxColourValue: string = '';

  //  Destination Country
  destinationCountryValue: string = '';

  countryObject: Country | undefined;

  // countryList: string[] = [];
  // countryMap: Map<string, number> = new Map();
  countries: Country[] = [];

  // Map the weight to price
  priceMap: Map<string, number> = new Map([
    ['1', 5],
    ['2', 15],
    ['5', 25],
    ['8', 50],
  ]);

  constructor(
    private fb: FormBuilder,
    private countryService: CountryApiService,
    private shipmentService: ShipmentApiService,
    // Declare the dialogue reference point
    public dialogRef: MatDialogRef<AddShipmentModalComponent>
  ) {}

  public getCountries(): void {
    this.countryService.getCountries().subscribe(
      (response: Country[]) => {
        this.countries = response;
      },
      (error: HttpErrorResponse) => console.log(error.message)
    );
  }

  public createShipment(): void {
    this.shipmentService.createShipment(this.addShipmentForm.value).subscribe(
      (response: Shipment) => {
        console.log('Added shipment');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  ngOnInit(): void {
    this.getCountries();
  }

  addShipmentForm = this.fb.group({
    receiver_name: [
      this.receiverNameValue,
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        // Not allowing numbers and symbols in name
        Validators.pattern('[^0-9!@#$%^&*]+'),
      ],
    ],

    weight: [this.weightValue, Validators.required],

    box_colour: [
      this.boxColourValue,
      // Custom validator to check if color
      // is valid hexa, rgb, or rgba
      [Validators.required, colorValidator(), Validators.maxLength(25)],
    ],

    destination_id: [this.destinationCountryValue, [Validators.required]],
    cost: [''],
    // date: [''],
  });

  get boxColour() {
    return this.addShipmentForm.get('box_colour');
  }

  get receiverName() {
    return this.addShipmentForm.get('receiver_name');
  }

  get destinationCountry() {
    return this.addShipmentForm.get('destination_id');
  }

  get weight() {
    return this.addShipmentForm.get('weight');
  }

  get price() {
    return this.addShipmentForm.get('price');
  }

  public onChangeColor(color: string): void {
    // example from documentation: form.patchValue({first: 'Nancy'});
    this.addShipmentForm.patchValue({ box_colour: color });
  }

  // Resets the form and closes the modal window
  resetAndCloseDialog() {
    // Reset the form to originally fetched API values
    this.boxColourValue = '';
    this.addShipmentForm.reset();
    this.dialogRef.close(); // close dialogue
  }

  onSubmit() {
    // Print the value in console
    console.log(this.addShipmentForm.value);

    // Async submit data to API
    this.createShipment();

    // Close the dialog
    this.resetAndCloseDialog();
  }

  get calcPrice() {
    // get the price using the weight and priceMap
    const cost = this.priceMap.get(this.addShipmentForm.value['weight']);
    // get the modifier by filtering the countries object list
    // and taking the modifier value of the first match
    const modifier = this.countries.filter(
      (country) => country.id == this.addShipmentForm.value['destination_id']
    )[0].modifier;

    if (modifier && cost) {
      // Calculate result
      // Round to two decimal places
      // and convert to string with 2 decimal points
      const result = (Math.round(modifier * cost * 100) / 100).toFixed(2);
      // Patch value in form
      this.addShipmentForm.patchValue({
        cost: result,
      });
      // Return result for html display
      return result;
    } else {
      // return null if modifier and cost do not exist
      return null;
    }
  }
}
