import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddShipmentFormComponent } from '../add-shipment-form/add-shipment-form.component';

@Component({
  selector: 'app-add-shipment-modal',
  templateUrl: './add-shipment-modal.component.html',
  styleUrls: ['./add-shipment-modal.component.css'],
})
export class AddShipmentModalComponent implements OnInit {
  constructor(public dialog: MatDialog) {}
  ngOnInit(): void {}

  openDialog(): void {
    // Open the dialogue on button click
    const dialogRef = this.dialog.open(AddShipmentFormComponent, {
      width: '250px',
    });
  }
}
