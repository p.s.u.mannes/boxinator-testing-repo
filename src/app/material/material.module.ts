import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';

const MaterialComponents = [
  MatIconModule,
  MatTableModule,
  MatButtonModule,
  MatDialogModule,
  MatRadioModule,
];

@NgModule({
  imports: [
    MatRadioModule,
    MatIconModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
  ],
  exports: [
    MatRadioModule,
    MatIconModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
  ],
})
export class MaterialModule {}
