import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

// Validate whether the color is a valid rgb(a) value
// Returns either error object or null if no errors
// exist
export function colorValidator(): ValidatorFn {
  // return validator fn
  return (control: AbstractControl): ValidationErrors | null => {
    let color = control.value; //grab the value from the control

    // if color exists
    if (color) {
      // Check if color matches hexacolor
      //   ^                 # start of the line
      //   #                 # start with a number sign `#`
      //   (                 # start of (group 1)
      //     [a-fA-F0-9]{6}  # support z-f, A-F and 0-9, with a length of 6
      //     |               # or
      //     [a-fA-F0-9]{3}  # support z-f, A-F and 0-9, with a length of 3
      //   )                 # end of (group 1)
      //   $                 # end of the line
      if (color.match('^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$')) {
        // matches, return nothing
        return null;
      }

      // Check if color matches rgb(a)
      // Valid values can contain whitespaces and tabspaces:
      // rgb(5345435,5345,345)

      // Regex at start
      const start = '^rgb(a)?\\((\\s*\\d+\\s*,){2}\\s*\\d+\\s*';
      // Regex in middle (can also be used as a boolean check
      // for if alpha exists)
      const alpha = color[3] == 'a' ? ',\\s*(1|0.\\d+)\\s*' : '';
      // Regex at end
      const end = '\\)$';

      if (color.match(start + alpha + end)) {
        // Take * inside rgb(*) or rgba(*) parenthesis
        color = alpha
          ? color.substring(5, color.length - 1)
          : color.substring(4, color.length - 1);

        // Remove whitespaces and split on commas
        const colors = color.replace(/\s/g, '').split(',');

        // Loop through colors array
        for (let x = 0; x < colors.length; x++) {
          if (x == 3) {
            if (colors[x] > 1) {
              // return error object
              return {
                maxalpha: true,
              };
            }
          } else {
            if (colors[x] > 255) {
              // return error object
              return {
                maxrgb: true,
              };
            }
          }
        }
        // Matched, return null
        return null;
      }

      // Return error if no successful matches
      // and color exists
      return {
        invalid: true,
      };
    }

    // Return null by default if color does not exist
    return null;
  };
}
