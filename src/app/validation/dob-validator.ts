import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

// Validate whether the DOB is in a valid range
// Min 16, Max 130
// Returns either error object or null if no errors
// exist
export function dobValidator(): ValidatorFn {
  // return validator fn
  return (control: AbstractControl): ValidationErrors | null => {
    let birthday = control.value; //grab the value from the control
    let currentYear = new Date().getFullYear();
    if (birthday) {
      birthday = parseInt(birthday.split('-')[0]);

      // check if person is older than 130
      if (birthday < currentYear - 130) {
        return {
          maxyear: true,
        };
      }

      // check if person is younger than 16
      if (birthday > currentYear - 16) {
        return {
          minyear: true,
        };
      }
    }
    return null;
  };
}
