export interface Shipment {
  id: number;
  receiver_name: string;
  weight: number;
  box_colour: string;
  destination_id: number;
  issue_date: string;
  status: string;
  cost: number;
  status_history: [
    {
      update_time: string;
      status: string;
    }
  ];
  account: {
    id: number;
    email: string;
  };
}

export interface editableShipment {
  isEdit: boolean;
  id: number;
  receiver_name: string;
  weight: number;
  box_colour: string;
  destination_id: number;
  issue_date: string;
  status: string;
  cost: number;
  status_history: [
    {
      update_time: string;
      status: string;
    }
  ];
  account: {
    id: number;
    email: string;
  };
}
