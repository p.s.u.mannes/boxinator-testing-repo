import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Shipment } from './shipment-interface';

@Injectable({
  providedIn: 'root',
})
export class ShipmentApiService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  public getShipments(): Observable<Shipment[]> {
    return this.http.get<Shipment[]>(`${this.apiServerUrl}/shipments`);
  }

  public getCompletedShipments(): Observable<Shipment[]> {
    return this.http.get<Shipment[]>(
      `${this.apiServerUrl}/shipments/completed`
    );
  }

  public getCancelledShipments(): Observable<Shipment[]> {
    return this.http.get<Shipment[]>(
      `${this.apiServerUrl}/shipments/cancelled`
    );
  }

  public getShipmentById(id: number): Observable<Shipment> {
    return this.http.get<Shipment>(`${this.apiServerUrl}/shipments/${id}`);
  }

  public updateShipment(
    shipmentId: number,
    shipment: Shipment
  ): Observable<Shipment> {
    return this.http.put<Shipment>(
      `${this.apiServerUrl}/shipments/${shipmentId}`,
      shipment
    );
  }

  public createShipment(shipment: Shipment): Observable<Shipment> {
    return this.http.post<Shipment>(`${this.apiServerUrl}/shipments`, shipment);
  }

  public createShipmentForGuest(shipment: Shipment): Observable<Shipment> {
    return this.http.post<Shipment>(
      `${this.apiServerUrl}/guest/shipments`,
      shipment
    );
  }
}
