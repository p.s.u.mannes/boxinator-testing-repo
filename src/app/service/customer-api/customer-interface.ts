export interface Customer {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  birthday: string;
  country_id: string;
  zip_code: string;
  phone_number: string;
  account_type: string;
}
