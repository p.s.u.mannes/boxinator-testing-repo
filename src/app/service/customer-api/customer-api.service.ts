import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Customer } from './customer-interface';

@Injectable({
  providedIn: 'root',
})
export class CustomerApiService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  //@TODO How to get customer by ID?
  /* http function takes in url and payload (optional second param) */

  public getCustomerByEmail(email: string): Observable<Customer> {
    return this.http.get<Customer>(
      `${this.apiServerUrl}/account?email=${email}`
    );
  }

  public getCustomer(id: number): Observable<Customer> {
    return this.http.get<Customer>(`${this.apiServerUrl}/account/${id}`);
  }

  public updateCustomer(id: number, customer: Customer): Observable<Customer> {
    return this.http.put<Customer>(
      `${this.apiServerUrl}/account/${id}`,
      customer
    );
  }

  public createCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(`${this.apiServerUrl}/account`, customer);
  }
}
