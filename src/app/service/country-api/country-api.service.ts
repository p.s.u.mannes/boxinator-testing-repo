import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Country } from './country-interface';

@Injectable({
  providedIn: 'root',
})
export class CountryApiService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  public getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(`${this.apiServerUrl}/settings/countries`);
  }

  public updateCountry(id: number, country: Country): Observable<Country> {
    return this.http.put<Country>(
      //'http://localhost:8081/api/v1/settings/countries/13'
      `${this.apiServerUrl}/settings/countries/${id}`,
      country
    );
  }
}
