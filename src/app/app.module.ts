import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';

import { initializeKeycloak } from './utility/app.init';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { MaterialModule } from './material/material.module';
import { AddGuestShipmentFormComponent } from './view/guest/add-guest-shipment-form/add-guest-shipment-form.component';
import { AdminModifiersComponent } from './view/admin/admin-modifiers/admin-modifiers.component';
import { AdminNavbarComponent } from './view/admin/admin-navbar/admin-navbar.component';
import { AdminMainPageComponent } from './view/pages/admin-main-page/admin-main-page.component';
import { FooterComponent } from './view/misc/footer/footer.component';
import { HeaderComponent } from './view/misc/header/header.component';
import { LogoutComponent } from './view/misc/logout/logout.component';
import { LoginPageComponent } from './view/pages/login-page/login-page.component';
import { UserMainPageComponent } from './view/pages/user-main-page/user-main-page.component';
import { GuestMainPageComponent } from './view/pages/guest-main-page/guest-main-page.component';
import { AdminModifierPageComponent } from './view/pages/admin-modifier-page/admin-modifier-page.component';
import { UserSettingsPageComponent } from './view/pages/user-settings-page/user-settings-page.component';
import { SettingsFormComponent } from './view/user/settings-form/settings-form.component';
import { ShipmentListComponent } from './view/user/shipment-list/shipment-list.component';
import { PackageStatusComponent } from './view/admin/package-status/package-status.component';
import { PackageStatusPageComponent } from './view/pages/package-status-page/package-status-page.component';
import { UserNavbarComponent } from './view/user/user-navbar/user-navbar.component';
import { AddShipmentModalComponent } from './view/user/add-shipment/add-shipment-modal/add-shipment-modal.component';
import { AddShipmentFormComponent } from './view/user/add-shipment/add-shipment-form/add-shipment-form.component';
import { AdminShipmentListComponent } from './view/admin/admin-shipment-list/admin-shipment-list.component';
import { ClickableIconComponent } from './view/misc/clickable-icon/clickable-icon.component';
import { FlashTextComponent } from './view/misc/flash-text/flash-text.component';
import { NavbarComponent } from './view/misc/navbar/navbar.component';
import { NavbarSettingsComponent } from './view/misc/navbar-settings/navbar-settings.component';
import { NavbarPackageHistoryComponent } from './view/misc/navbar-package-history/navbar-package-history.component';

@NgModule({
  declarations: [
    AppComponent,
    AddGuestShipmentFormComponent,
    AdminModifiersComponent,
    AdminNavbarComponent,
    AdminMainPageComponent,
    FooterComponent,
    HeaderComponent,
    LogoutComponent,
    SettingsFormComponent,
    GuestMainPageComponent,
    LoginPageComponent,
    UserMainPageComponent,
    LoginPageComponent,
    AdminModifierPageComponent,
    UserSettingsPageComponent,
    ShipmentListComponent,
    PackageStatusComponent,
    PackageStatusPageComponent,
    UserNavbarComponent,
    LogoutComponent,
    AddShipmentModalComponent,
    AddShipmentFormComponent,
    AdminShipmentListComponent,
    ClickableIconComponent,
    FlashTextComponent,
    NavbarComponent,
    NavbarSettingsComponent,
    NavbarPackageHistoryComponent,
  ],
  imports: [
    MaterialModule,
    ColorPickerModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    KeycloakAngularModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
