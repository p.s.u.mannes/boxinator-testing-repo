# Boxinator 

- The application gives access to a portal where users can create orders for mysterious boxes to be shipped to addresses around the world. 

## Intructions to run the application

- npm install
- ng serve
- Application runs on PORT:4200


## To Login 

- [Click here to discover!](https://boxinator-web.herokuapp.com/welcome)
- In order to test the application functionalities you can create an user. For test as an administrator, we recommend you to use the following user:
 Email: admin@mail.com
 Password:Admin@123


## Front-End Repository

- [Check our server repository](https://gitlab.com/camilamboaventura/boxinator-server)

## Follow the Dev Team

- [Camila Boaventura](https://gitlab.com/camilamboaventura)  

- [Philip Mannes](https://gitlab.com/p.s.u.mannes) 

